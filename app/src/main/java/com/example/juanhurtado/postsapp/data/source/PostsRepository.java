package com.example.juanhurtado.postsapp.data.source;

import com.example.juanhurtado.postsapp.data.Comment;
import com.example.juanhurtado.postsapp.data.Post;
import com.example.juanhurtado.postsapp.data.User;
import com.example.juanhurtado.postsapp.data.source.local.LocalDataSource;
import com.example.juanhurtado.postsapp.data.source.remote.RemoteDataSource;
import com.example.juanhurtado.postsapp.data.webservice.BaseCallback;
import com.example.juanhurtado.postsapp.data.webservice.ServiceError;

import java.util.List;

import io.realm.Realm;

/**
 * Created by Juan Hurtado on 5/06/2018.
 */

public class PostsRepository implements DataSource {
    private static PostsRepository INSTANCE;
    private DataSource remoteDataSource;
    private DataSource localDataSource;

    private PostsRepository(RemoteDataSource remoteDataSource, LocalDataSource localDataSource) {
        this.remoteDataSource = remoteDataSource;
        this.localDataSource = localDataSource;

    }

    public static PostsRepository getInstance(RemoteDataSource remoteDataSource, LocalDataSource localDataSource) {
        if (INSTANCE == null)
            INSTANCE = new PostsRepository(remoteDataSource,localDataSource);

        return INSTANCE;
    }

    @Override
    public void refreshPosts(final BaseCallback<List<Post>> callback) {
        remoteDataSource.refreshPosts(new BaseCallback<List<Post>>() {
            @Override
            public void onSuccess(List<Post> getPostLists) {
                localDataSource.saveAllPosts(getPostLists);
                callback.onSuccess(getPostLists);
            }

            @Override
            public void onFailure(ServiceError serviceError) {
                callback.onFailure(serviceError);
            }
        });
    }

    @Override
    public void getAllPosts(BaseCallback<List<Post>> callback) {
        if(Realm.getDefaultInstance().isEmpty()) {
            refreshPosts(callback);
        }else{
            localDataSource.getAllPosts(callback);
        }
    }

    @Override
    public void getFavoritePosts(BaseCallback<List<Post>> callback) {
        localDataSource.getFavoritePosts(callback);
    }

    @Override
    public void saveAllPosts(List<Post> postList) {
        localDataSource.saveAllPosts(postList);
    }

    @Override
    public void getUserInformation(final BaseCallback<List<User>> callback, int userId) {
        remoteDataSource.getUserInformation(new BaseCallback<List<User>>() {
            @Override
            public void onSuccess(List<User> user) {
                saveUserInformation(user.get(0));
                callback.onSuccess(user);
            }

            @Override
            public void onFailure(ServiceError serviceError) {
                callback.onFailure(serviceError);
            }
        },userId);
    }

    @Override
    public void getComments(final BaseCallback<List<Comment>> callback, int postId) {
        remoteDataSource.getComments(new BaseCallback<List<Comment>>() {
            @Override
            public void onSuccess(List<Comment> commentList) {
                saveAllComments(commentList);
                callback.onSuccess(commentList);
            }

            @Override
            public void onFailure(ServiceError serviceError) {
                callback.onFailure(serviceError);
            }
        },postId);
    }

    @Override
    public void saveUserInformation(User user) {
        localDataSource.saveUserInformation(user);
    }

    @Override
    public void saveAllComments(List<Comment> commentList) {
        localDataSource.saveAllComments(commentList);
    }

    @Override
    public void updatePost(Post post) {
        localDataSource.updatePost(post);
    }

    @Override
    public void removeAllPosts() {
        localDataSource.removeAllPosts();
    }

    @Override
    public void removePost(Post post) {
        localDataSource.removePost(post);
    }
}
