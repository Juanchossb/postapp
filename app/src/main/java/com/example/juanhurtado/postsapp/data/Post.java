package com.example.juanhurtado.postsapp.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Juan Hurtado on 5/06/2018.
 */

public class Post extends RealmObject implements Parcelable{
    @SerializedName("userId")
    private int userId;
    @SerializedName("id")
    @PrimaryKey
    private int id;
    @SerializedName("title")
    private String title;
    @SerializedName("body")
    private String body;
    private boolean isFavorite;
    private boolean wasRead = true;

    protected Post(Parcel in) {
        userId = in.readInt();
        id = in.readInt();
        title = in.readString();
        body = in.readString();
        isFavorite = in.readByte() != 0;
        wasRead = in.readByte() != 0;
    }
    public Post(){}

    public static final Creator<Post> CREATOR = new Creator<Post>() {
        @Override
        public Post createFromParcel(Parcel in) {
            return new Post(in);
        }

        @Override
        public Post[] newArray(int size) {
            return new Post[size];
        }
    };

    public int getUserId(){
        return userId;
    }

    public int getId(){
        return id;
    }

    public String getTitle(){
        return title;
    }

    public String getBody(){
        return body;
    }

    public void setFavorite(boolean favorite){
        isFavorite = favorite;
    }

    public boolean isFavorite(){
        return isFavorite;
    }

    public  void setRead(boolean read){
        wasRead = read;
    }
    public boolean wasRead(){
        return wasRead;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(userId);
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(body);
        dest.writeByte((byte) (isFavorite ? 1 : 0));
        dest.writeByte((byte) (wasRead ? 1 : 0));
    }
}
