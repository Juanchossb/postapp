package com.example.juanhurtado.postsapp.data.webservice;

/**
 * Created by Juan Hurtado on 5/06/2018.
 */

public interface BaseCallback<T> {
    void onSuccess(T t);
    void onFailure(ServiceError serviceError);
}
