package com.example.juanhurtado.postsapp.posts;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.example.juanhurtado.postsapp.R;
import com.example.juanhurtado.postsapp.data.Post;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

/**
 * Created by Juan Hurtado on 5/06/2018.
 */

public class PostsActivity extends AppCompatActivity implements PostListFragment.PostListListener{

    @BindView(R.id.posts_tabs)
    TabLayout tabLayout;
    @BindView(R.id.tabs_view_pager)
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posts);
        ButterKnife.bind(this);
        Realm.init(this);
        initializeTabs();
    }

    private void initializeTabs(){

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(tabSelectedListener);

        PostViewPagerAdapter viewPagerAdapter = new PostViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(PostListFragment.newInstance(PostListFragment.Action.ALL_POSTS),getString(R.string.tab_title_all_notifications));
        viewPagerAdapter.addFragment(PostListFragment.newInstance(PostListFragment.Action.FAVORITE_POSTS),getString(R.string.tab_title_favorite_notification));
        viewPager.setAdapter(viewPagerAdapter);
    }

    private TabLayout.OnTabSelectedListener tabSelectedListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            switch (tab.getPosition()){
                case 0:
                    showAllPostsFragment();
                    break;
                case 1:
                    showFavoritePostsFragment();
                    break;
            }
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };

    @Override
    public void showFavoritePostsFragment() {
        viewPager.setCurrentItem(1);
    }

    @Override
    public void showAllPostsFragment() {
        viewPager.setCurrentItem(0);
    }

    @Override
    public void launchPostDetailAcivity(Post post) {
        Intent intent = new Intent(this,PostDetailActivity.class);
        intent.putExtra(Post.class.getSimpleName(),post);
        startActivity(intent);
    }
}
