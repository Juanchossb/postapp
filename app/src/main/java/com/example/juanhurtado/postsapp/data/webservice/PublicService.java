package com.example.juanhurtado.postsapp.data.webservice;

import com.example.juanhurtado.postsapp.data.Comment;
import com.example.juanhurtado.postsapp.data.Post;
import com.example.juanhurtado.postsapp.data.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Juan Hurtado on 5/06/2018.
 */

public interface PublicService {

    String POSTS = "posts/";
    String COMMENTS ="comments";
    String USERS = "users";

    @GET(POSTS)
    Call<List<Post>> getPostsList();

    @GET(COMMENTS)
    Call<List<Comment>> getCommentList(@Query("postId") int postId);

    @GET(USERS)
    Call<List<User>> getUserInformation(@Query("id") int userId);


}
