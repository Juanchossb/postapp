package com.example.juanhurtado.postsapp.posts;

import com.example.juanhurtado.postsapp.BaseView;
import com.example.juanhurtado.postsapp.data.Post;

import java.util.List;

/**
 * Created by Juan Hurtado on 5/06/2018.
 */

public class PostsContract<T> {

    interface View extends BaseView<Presenter> {
        void updatePostsList(List<Post> postList);
        void onPostClicked(Post post);
    }

    public interface Presenter{
        void start();
        void getAllPostsList();
        void getFavoritePostList();
        void refreshPostList();

        void removeAllPosts();
        void removePost(Post post);
    }
}
