package com.example.juanhurtado.postsapp.data.source.remote;

import com.example.juanhurtado.postsapp.Injection;
import com.example.juanhurtado.postsapp.data.Comment;
import com.example.juanhurtado.postsapp.data.Post;
import com.example.juanhurtado.postsapp.data.User;
import com.example.juanhurtado.postsapp.data.source.DataSource;
import com.example.juanhurtado.postsapp.data.webservice.BaseCallback;
import com.example.juanhurtado.postsapp.data.webservice.PublicService;
import com.example.juanhurtado.postsapp.data.webservice.ServiceError;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Juan Hurtado on 5/06/2018.
 */

public class RemoteDataSource implements DataSource {
    private static RemoteDataSource INSTANCE;
    private PublicService publicService;
    private static final int AMOUNT_UNREAD = 20;

    public static RemoteDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RemoteDataSource();
            INSTANCE.publicService = Injection.provideRetrofitService();
        }

        return INSTANCE;
    }

    @Override
    public void refreshPosts(BaseCallback<List<Post>> callback) {
        getAllPosts(callback);
    }

    @Override
    public void getAllPosts(final BaseCallback<List<Post>> callback) {
        Call<List<Post>> getPostListsCall = publicService.getPostsList();
        getPostListsCall.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                callback.onSuccess(addUnreadStatus(response.body()));
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                callback.onFailure(new ServiceError(t.getMessage()));
            }
        });
    }

    @Override
    public void getFavoritePosts(BaseCallback<List<Post>> callback) {

    }

    @Override
    public void saveAllPosts(List<Post> postList) {
    }

    @Override
    public void getUserInformation(final BaseCallback<List<User>> callback, int userId) {
        Call<List<User>> userInformation = publicService.getUserInformation(userId);
        userInformation.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                callback.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                callback.onFailure(new ServiceError(t.getMessage()));
            }
        });
    }

    @Override
    public void getComments(final BaseCallback<List<Comment>> callback, int postId) {
        Call<List<Comment>> commentCall = publicService.getCommentList(postId);
        commentCall.enqueue(new Callback<List<Comment>>() {
            @Override
            public void onResponse(Call<List<Comment>> call, Response<List<Comment>> response) {
                callback.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<List<Comment>> call, Throwable t) {
                callback.onFailure(new ServiceError(t.getMessage()));
            }
        });

    }

    @Override
    public void saveUserInformation(User user) {

    }

    @Override
    public void saveAllComments(List<Comment> commentList) {

    }

    @Override
    public void updatePost(Post post) {

    }

    @Override
    public void removeAllPosts() {

    }

    @Override
    public void removePost(Post post) {

    }

    private List<Post> addUnreadStatus(List<Post> postList) {
        for (int i = 0; i < AMOUNT_UNREAD; i++) {
            if (i < postList.size())
                postList.get(i).setRead(false);
        }
        return postList;
    }
}
