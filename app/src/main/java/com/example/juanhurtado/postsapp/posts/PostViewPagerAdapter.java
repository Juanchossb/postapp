package com.example.juanhurtado.postsapp.posts;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Juan Hurtado on 5/06/2018.
 */

public class PostViewPagerAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragmentList = new ArrayList<>();
    private List<String> titleList = new ArrayList<>();
    private static final String TITLE = "title";

    public PostViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentList.get(position).getArguments().getString(TITLE);

    }

    public void addFragment(Fragment fragment, String title) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(TITLE, title);
        fragment.setArguments(bundle);
        fragmentList.add(fragment);
        titleList.add(title);
        notifyDataSetChanged();
    }
}
