package com.example.juanhurtado.postsapp.data;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by Juan Hurtado on 6/06/2018.
 */

public class Comment extends RealmObject{
    @SerializedName("postId")
    int postId;
    @SerializedName("id")
    int id;
    @SerializedName("name")
    String name;
    @SerializedName("email")
    String email;
    @SerializedName("body")
    String body;

    public String getBody(){
        return body;
    }
}
