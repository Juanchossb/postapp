package com.example.juanhurtado.postsapp.data.source.local;

import com.example.juanhurtado.postsapp.data.Comment;
import com.example.juanhurtado.postsapp.data.Post;
import com.example.juanhurtado.postsapp.data.User;
import com.example.juanhurtado.postsapp.data.source.DataSource;
import com.example.juanhurtado.postsapp.data.webservice.BaseCallback;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Juan Hurtado on 6/06/2018.
 */

public class LocalDataSource implements DataSource {
    private static LocalDataSource INSTANCE;
    Realm realm;

    public static LocalDataSource getInstance() {
        if (INSTANCE == null)
            INSTANCE = new LocalDataSource();
        INSTANCE.realm = Realm.getDefaultInstance();

        return INSTANCE;
    }

    @Override
    public void refreshPosts(BaseCallback<List<Post>> callback) {

    }

    @Override
    public void getAllPosts(final BaseCallback<List<Post>> callback) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Post> results = realm.where(Post.class).findAll();
                callback.onSuccess(results);
            }
        });

    }

    @Override
    public void getFavoritePosts(final BaseCallback<List<Post>> callback) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Post> results = realm.where(Post.class).equalTo("isFavorite", true).findAll();
                callback.onSuccess(results);
            }
        });
    }

    @Override
    public void saveAllPosts(final List<Post> postList) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Post> results = realm.where(Post.class).findAll();
                results.deleteAllFromRealm();
                realm.insert(postList);
            }
        });
    }

    @Override
    public void getUserInformation(final BaseCallback<List<User>> callback, final int userId) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                List<User> results = realm.where(User.class).equalTo("id", userId).findAll();
                callback.onSuccess(results);
            }
        });
    }

    @Override
    public void getComments(final BaseCallback<List<Comment>> callback, final int postId) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Comment> results = realm.where(Comment.class).equalTo("postId", postId).findAll();
                callback.onSuccess(results);
            }
        });

    }

    @Override
    public void saveUserInformation(final User user) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insert(user);
            }
        });
    }

    @Override
    public void saveAllComments(final List<Comment> commentList) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insert(commentList);
            }
        });
    }

    @Override
    public void updatePost(final Post post) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Post result = realm.where(Post.class).equalTo("id", post.getId()).findFirst();
                result.setRead(post.wasRead());
                result.setFavorite(post.isFavorite());
                realm.copyToRealmOrUpdate(result);
            }
        });
    }

    @Override
    public void removeAllPosts() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }

    @Override
    public void removePost(final Post post) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Post> results = realm.where(Post.class).equalTo("id", post.getId()).findAll();
                results.deleteAllFromRealm();
            }
        });
    }
}
