package com.example.juanhurtado.postsapp.posts;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.juanhurtado.postsapp.R;
import com.example.juanhurtado.postsapp.data.Post;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Juan Hurtado on 5/06/2018.
 */

public class PostListAdapter extends RecyclerView.Adapter<PostListAdapter.PostListViewHolder> {

    private List<Post> postList = new ArrayList<>();
    private PostsContract.View postContractView;

    public static PostListAdapter getInstance(PostsContract.View posttContractView) {

        PostListAdapter adapter = new PostListAdapter();
        adapter.postContractView = posttContractView;

        return adapter;
    }

    public void setPostList(List<Post> postList) {
        this.postList = postList;
    }

    public List<Post> getPostList() {
        return postList;
    }

    public void removeItemAt(int position) {
        // ((RealmResults)postList).deleteFromRealm(position);

        notifyItemRemoved(position);
    }

    @NonNull
    @Override
    public PostListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_list_item, parent, false);
        return new PostListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final PostListViewHolder holder, int position) {
        final Post post = postList.get(position);
        holder.tvTitle.setText(post.getTitle());
        holder.ivFavorite.setVisibility(post.isFavorite() ? View.VISIBLE : View.GONE);
        holder.ivReadDot.setVisibility(post.wasRead() ? View.GONE : View.VISIBLE);
        ((View) holder.tvTitle.getParent()).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postContractView.onPostClicked(post);
            }
        });
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    public class PostListViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_read_dot)
        ImageView ivReadDot;
        @BindView(R.id.image_favorite)
        ImageView ivFavorite;
        @BindView(R.id.text_title)
        TextView tvTitle;

        public PostListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
