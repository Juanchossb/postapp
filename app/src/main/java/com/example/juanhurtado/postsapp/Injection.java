package com.example.juanhurtado.postsapp;

import android.content.Context;

import com.example.juanhurtado.postsapp.data.source.PostsRepository;
import com.example.juanhurtado.postsapp.data.source.local.LocalDataSource;
import com.example.juanhurtado.postsapp.data.source.remote.RemoteDataSource;
import com.example.juanhurtado.postsapp.data.webservice.PublicService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Juan Hurtado on 5/06/2018.
 */

public class Injection {

    private static final String URL = "https://jsonplaceholder.typicode.com/";

    public static PublicService provideRetrofitService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(PublicService.class);
    }

    public static PostsRepository providePostsRepository(Context context) {
        return PostsRepository.getInstance(RemoteDataSource.getInstance(), LocalDataSource.getInstance());
    }

}
