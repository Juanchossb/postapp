package com.example.juanhurtado.postsapp.data.source;

import com.example.juanhurtado.postsapp.data.Comment;
import com.example.juanhurtado.postsapp.data.Post;
import com.example.juanhurtado.postsapp.data.User;
import com.example.juanhurtado.postsapp.data.webservice.BaseCallback;

import java.util.List;

/**
 * Created by Juan Hurtado on 5/06/2018.
 */

public interface DataSource {
    void refreshPosts(BaseCallback<List<Post>> callback);
    void getAllPosts(BaseCallback<List<Post>> callback);
    void getFavoritePosts(BaseCallback<List<Post>> callback);
    void saveAllPosts(List<Post> postList);
    void getUserInformation(BaseCallback<List<User>> callback, int userId);
    void getComments(BaseCallback<List<Comment>> callback, int postId);
    void saveUserInformation(User user);
    void saveAllComments(List<Comment> commentList);
    void updatePost(Post post);

    void removeAllPosts();
    void removePost(Post post);
}
