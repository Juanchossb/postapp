package com.example.juanhurtado.postsapp;

/**
 * Created by Juan Hurtado on 5/06/2018.
 */

public interface BaseView<T> {
    void setPresenter(T presenter);
}
