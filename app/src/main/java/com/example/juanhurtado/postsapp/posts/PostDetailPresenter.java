package com.example.juanhurtado.postsapp.posts;

import android.util.Log;

import com.example.juanhurtado.postsapp.data.Comment;
import com.example.juanhurtado.postsapp.data.Post;
import com.example.juanhurtado.postsapp.data.User;
import com.example.juanhurtado.postsapp.data.source.PostsRepository;
import com.example.juanhurtado.postsapp.data.webservice.BaseCallback;
import com.example.juanhurtado.postsapp.data.webservice.ServiceError;

import java.util.List;

/**
 * Created by Juan Hurtado on 6/06/2018.
 */

public class PostDetailPresenter implements PostDetailContract.Presenter {

    private PostsRepository postsRepository;
    private PostDetailContract.View view;
    private static final String TAG = PostDetailPresenter.class.getSimpleName();

    public PostDetailPresenter(PostsRepository postsRepository, PostDetailContract.View view) {
        this.postsRepository = postsRepository;
        this.view = view;
        view.setPresenter(this);
    }

    @Override
    public void getUserInformation(int userId) {
        postsRepository.getUserInformation(new BaseCallback<List<User>>() {
            @Override
            public void onSuccess(List<User> users) {
                view.updateUserInformation(users.get(0));
            }

            @Override
            public void onFailure(ServiceError serviceError) {
                Log.e(TAG, serviceError.getMessage());
            }
        }, userId);
    }

    @Override
    public void getComments(int postId) {
        postsRepository.getComments(new BaseCallback<List<Comment>>() {
            @Override
            public void onSuccess(List<Comment> commentList) {
                view.updateComments(commentList);
            }

            @Override
            public void onFailure(ServiceError serviceError) {
                Log.e(TAG, serviceError.getMessage());
            }
        }, postId);
    }

    @Override
    public void updatePost(Post post) {
        postsRepository.updatePost(post);
    }
}
