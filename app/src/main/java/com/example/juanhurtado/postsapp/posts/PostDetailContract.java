package com.example.juanhurtado.postsapp.posts;

import com.example.juanhurtado.postsapp.BaseView;
import com.example.juanhurtado.postsapp.data.Comment;
import com.example.juanhurtado.postsapp.data.Post;
import com.example.juanhurtado.postsapp.data.User;

import java.util.List;

/**
 * Created by Juan Hurtado on 6/06/2018.
 */

public class PostDetailContract<T> {
    interface View extends BaseView<Presenter> {
        void updateUserInformation(User user);

        void updateComments(List<Comment> commentList);
    }

    interface Presenter {
        void getUserInformation(int userId);

        void getComments(int postId);

        void updatePost(Post post);

    }
}
