package com.example.juanhurtado.postsapp.posts;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.juanhurtado.postsapp.R;
import com.example.juanhurtado.postsapp.data.Comment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Juan Hurtado on 6/06/2018.
 */

public class PostDetailAdapter extends RecyclerView.Adapter<PostDetailAdapter.PostDetailViewHolder> {

    private List<Comment> commentList = new ArrayList<>();
    private PostDetailContract.View view;
    private static PostDetailAdapter INSTANCE;

    public static PostDetailAdapter getInstance(PostDetailContract.View view) {
        if (INSTANCE == null)
            INSTANCE = new PostDetailAdapter();
        INSTANCE.view = view;
        return INSTANCE;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }

    @NonNull
    @Override
    public PostDetailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_detail_item, parent, false);
        return new PostDetailAdapter.PostDetailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PostDetailViewHolder holder, int position) {
        Comment comment = commentList.get(position);
        holder.tvDetail.setText(comment.getBody());

    }

    @Override
    public int getItemCount() {
        return commentList.size();
    }

    public class PostDetailViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.post_detail_text)
        TextView tvDetail;

        public PostDetailViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
