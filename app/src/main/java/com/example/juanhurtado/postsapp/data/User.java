package com.example.juanhurtado.postsapp.data;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by Juan Hurtado on 5/06/2018.
 */

public class User extends RealmObject{
    @SerializedName("id")
    private int id;
    @SerializedName("username")
    private String username;
    @SerializedName("email")
    private String email;
    @SerializedName("phone")
    private String phone;
    @SerializedName("website")
    private String website;

    public int getId(){
        return id;
    }

    public String getUsername(){
        return username;
    }

    public String getEmail(){
        return email;
    }

    public String getPhone(){
        return phone;
    }

    public String getWebsite(){
        return website;
    }
}
