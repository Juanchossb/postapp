package com.example.juanhurtado.postsapp.posts;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;

import com.example.juanhurtado.postsapp.R;
import com.example.juanhurtado.postsapp.data.Post;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

/**
 * Created by Juan Hurtado on 6/06/2018.
 */

public class PostDetailActivity extends AppCompatActivity {

    @BindView(R.id.post_detail_container)
    LinearLayout container;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_detail);
        ButterKnife.bind(this);
        Realm.init(this);
        Post post = (Post) getIntent().getParcelableExtra(Post.class.getSimpleName());
        initializeDetailFragment(post);
    }

    private void initializeDetailFragment(Post post) {
        PostDetailFragment fragment = PostDetailFragment.newInstance(post);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.post_detail_container, fragment);
        ft.commit();
    }
}
