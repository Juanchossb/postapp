package com.example.juanhurtado.postsapp.posts;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.juanhurtado.postsapp.Injection;
import com.example.juanhurtado.postsapp.R;
import com.example.juanhurtado.postsapp.data.Comment;
import com.example.juanhurtado.postsapp.data.Post;
import com.example.juanhurtado.postsapp.data.User;
import com.example.juanhurtado.postsapp.data.source.PostsRepository;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Juan Hurtado on 6/06/2018.
 */

public class PostDetailFragment extends Fragment implements PostDetailContract.View {
    @BindView(R.id.post_description)
    TextView tvDescription;
    @BindView(R.id.post_user_name)
    TextView tvUserName;
    @BindView(R.id.post_user_email)
    TextView tvUserEmail;
    @BindView(R.id.post_user_phone)
    TextView tvUserPhone;
    @BindView(R.id.post_user_website)
    TextView tvUserWebsite;
    @BindView(R.id.post_comment_list_view)
    RecyclerView rvPostComments;
    private PostDetailContract.Presenter presenter;
    private PostDetailAdapter adapter = PostDetailAdapter.getInstance(this);
    private Post post;

    public static PostDetailFragment newInstance(Post post) {
        PostDetailFragment postDetailFragment = new PostDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(Post.class.getSimpleName(), post);
        postDetailFragment.setArguments(bundle);
        postDetailFragment.setHasOptionsMenu(true);
        return postDetailFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_post_detail, container, false);
        ButterKnife.bind(this, fragmentView);
        return fragmentView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        PostsRepository postsRepository = Injection.providePostsRepository(getContext());
        new PostDetailPresenter(postsRepository, this);
        post = (Post) getArguments().getParcelable(Post.class.getSimpleName());
        post.setRead(true);
        presenter.updatePost(post);
        presenter.getUserInformation(post.getUserId());
        presenter.getComments(post.getId());
        tvDescription.setText(post.getBody());
        rvPostComments.setAdapter(adapter);
        rvPostComments.setLayoutManager(new LinearLayoutManager(getContext()));
        DividerItemDecoration itemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        rvPostComments.addItemDecoration(itemDecoration);

        ((PostDetailActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((PostDetailActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.main_actionbar, menu);
        menu.findItem(R.id.action_refresh).setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_favorite) {
            post.setFavorite(!post.isFavorite());
            presenter.updatePost(post);
            item.setIcon(post.isFavorite() ? R.drawable.ic_star : R.drawable.ic_star_border);
        } else if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }
        return true;
    }

    @Override
    public void setPresenter(PostDetailContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void updateUserInformation(User user) {
        tvUserName.setText(user.getUsername());
        tvUserEmail.setText(user.getEmail());
        tvUserPhone.setText(user.getPhone());
        tvUserWebsite.setText(user.getWebsite());
    }

    @Override
    public void updateComments(List<Comment> commentList) {
        adapter.setCommentList(commentList);
        adapter.notifyDataSetChanged();
    }
}
