package com.example.juanhurtado.postsapp.posts;

import android.util.Log;

import com.example.juanhurtado.postsapp.data.Post;
import com.example.juanhurtado.postsapp.data.source.PostsRepository;
import com.example.juanhurtado.postsapp.data.webservice.BaseCallback;
import com.example.juanhurtado.postsapp.data.webservice.ServiceError;

import java.util.List;

/**
 * Created by Juan Hurtado on 5/06/2018.
 */

public class PostsPresenter implements PostsContract.Presenter {

    private final PostsRepository postsRepository;
    private final PostsContract.View view;
    private static final String TAG = PostsPresenter.class.getSimpleName();

    PostsPresenter(PostsRepository postsRepository, PostsContract.View view) {
        this.postsRepository = postsRepository;
        this.view = view;
        view.setPresenter(this);
    }

    @Override
    public void start() {

    }

    @Override
    public void getAllPostsList() {
        postsRepository.getAllPosts(new BaseCallback<List<Post>>() {
            @Override
            public void onSuccess(List<Post> getPostLists) {
                view.updatePostsList(getPostLists);
            }

            @Override
            public void onFailure(ServiceError serviceError) {
                Log.e(TAG, serviceError.getMessage());
            }
        });
    }

    @Override
    public void getFavoritePostList() {
        postsRepository.getFavoritePosts(new BaseCallback<List<Post>>() {
            @Override
            public void onSuccess(List<Post> postList) {
                view.updatePostsList(postList);
            }

            @Override
            public void onFailure(ServiceError serviceError) {
                Log.e(TAG, serviceError.getMessage());
            }
        });
    }

    @Override
    public void refreshPostList() {
        postsRepository.refreshPosts(new BaseCallback<List<Post>>() {
            @Override
            public void onSuccess(List<Post> postList) {
                view.updatePostsList(postList);
            }

            @Override
            public void onFailure(ServiceError serviceError) {
                Log.e(TAG, serviceError.getMessage());
            }
        });
    }

    @Override
    public void removeAllPosts() {

    }

    @Override
    public void removePost(Post post) {
        postsRepository.removePost(post);
    }

}
