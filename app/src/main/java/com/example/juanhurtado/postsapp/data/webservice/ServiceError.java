package com.example.juanhurtado.postsapp.data.webservice;

/**
 * Created by Juan Hurtado on 5/06/2018.
 */

public class ServiceError {
    private String message;

    public ServiceError(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
