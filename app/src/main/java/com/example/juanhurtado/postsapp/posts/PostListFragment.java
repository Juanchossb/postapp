package com.example.juanhurtado.postsapp.posts;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.juanhurtado.postsapp.Injection;
import com.example.juanhurtado.postsapp.R;
import com.example.juanhurtado.postsapp.data.Post;
import com.example.juanhurtado.postsapp.data.source.PostsRepository;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Juan Hurtado on 5/06/2018.
 */

public class PostListFragment extends Fragment implements PostsContract.View {

    enum Action {ALL_POSTS, FAVORITE_POSTS}

    @BindView(R.id.post_list_view)
    RecyclerView postListView;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    protected PostsContract.Presenter presenter;
    private PostListAdapter postListAdapter = PostListAdapter.getInstance(this);
    private Action action;
    private PostListListener listListener;

    public static PostListFragment newInstance(Action action) {
        PostListFragment fragment = new PostListFragment();
        fragment.action = action;
        fragment.setHasOptionsMenu(true);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_posts_list, container, false);
        ButterKnife.bind(this, fragmentView);
        return fragmentView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        postListView.setLayoutManager(new LinearLayoutManager(getContext()));
        postListView.setAdapter(postListAdapter);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        postListView.addItemDecoration(itemDecoration);
        PostsRepository postsRepository = Injection.providePostsRepository(getContext());
        new PostsPresenter(postsRepository, this);
        new ItemTouchHelper(itemTouchHelperCallBack).attachToRecyclerView(postListView);
    }

    @Override
    public void onResume() {
        super.onResume();
        switch (action) {
            case ALL_POSTS:
                presenter.getAllPostsList();
                break;
            case FAVORITE_POSTS:
                presenter.getFavoritePostList();
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof PostListListener)
            listListener = (PostListListener) context;
    }

    @OnClick(R.id.fab)
    void floatingButtonClicked(View view) {
        presenter.removeAllPosts();
        postListAdapter.setPostList(new ArrayList<Post>());
        postListAdapter.notifyDataSetChanged();
    }

    @Override
    public void setPresenter(PostsContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void updatePostsList(List<Post> postList) {
        postListAdapter.setPostList(postList);
        postListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPostClicked(Post post) {
        listListener.launchPostDetailAcivity(post);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.main_actionbar, menu);
        menu.findItem(R.id.action_favorite).setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_refresh) {
            if (action == Action.ALL_POSTS)
                presenter.refreshPostList();
        }
        return true;
    }

    ItemTouchHelper.SimpleCallback itemTouchHelperCallBack = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            presenter.removePost(postListAdapter.getPostList().get(viewHolder.getAdapterPosition()));
            postListAdapter.notifyItemRemoved(viewHolder.getAdapterPosition());
        }
    };

    public interface PostListListener {
        void showFavoritePostsFragment();

        void showAllPostsFragment();

        void launchPostDetailAcivity(Post post);
    }

}
