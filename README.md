# Posts App

This app displays the post found in https://jsonplaceholder.typicode.com/ . It also allows the user to select favorite posts and check comments and user information


## Prerequisites

* Android SDK v27
* Latest Android Build Tools
* Android Support Laboratory

### Getting Started

This app uses the Gradle build system

* Download the sample by cloning this repository
* In Android Studio, create a new project and choose the "Import non-Android Studio project" or "Import Project" option
* Select the postapp directory that you downloaded with this repository

## Authors

* **Juan Hurtado** - *Development* - [Bitbucket](https://bitbucket.org/Juanchossb)